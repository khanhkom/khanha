import { DATA_TOPIC } from "../config/default";

const defaultState = {
	selectedTopic:DATA_TOPIC
};
export default (state = defaultState, action) => {
	switch (action.type) {
		case 'CHANGE_TOPIC': {
			let selectedTopic=state.selectedTopic;
			selectedTopic[action.id].selected=!selectedTopic[action.id].selected;
			return {
			...state,
			selectedTopic
		};
	}
		default:
			break;
	}
	return state;
};