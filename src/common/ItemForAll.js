import React, { Component } from 'react'
import {
    Text, View, StyleSheet,
} from 'react-native'
import { Card, Image, Subtitle, Caption, Button, Icon, TouchableOpacity } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ItemForAll = (props: any) => {
    const { content, onButton } = props;
    return (
        <TouchableOpacity
            onPress={()=>onButton()}
            style={{
                marginVertical:HEIGHT(5),
                width:WIDTH(360), paddingHorizontal:WIDTH(10),
                justifyContent: 'center',
                alignItems: 'center',
                }}
        >
            <View style={{
                flex:0,
                borderBottomRightRadius:WIDTH(5),
                borderBottomLeftRadius:WIDTH(5),
                backgroundColor:'#ffff',
                elevation:3,
               
            }}>
                <View style={{
                    flex:0,
                }}>
                        <Image
                            style={{
                                width:WIDTH(340),
                                height:HEIGHT(200),
                            }}
                            source={{ uri: content.imageMain }}
                        />
                </View>
                
                <View style={{
                    flex:0,
                    paddingHorizontal:WIDTH(10)
                }}>
                    <Subtitle
                        numberOfLines={1}
                    >{content.namePlace}</Subtitle>
                    <Caption>{content.address}</Caption>
                    <Caption>{content.time}</Caption>
                </View>
            </View>
        </TouchableOpacity>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

