import React, { Component } from 'react'
import {
    Text, StyleSheet,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { View, NavigationBar, Title, ImageBackground, Caption, Button, } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
    content: string,
    onButton: Function,
}
export default ItemForDetails = (props: any) => {
    const { content,linkImage, onButton } = props;
    return (
        <ImageBackground
            source={{ uri: linkImage }}
            style={{ width: WIDTH(360), height: HEIGHT(220) }}
        >
            <NavigationBar
                styleName="clear"
                leftComponent={(
                    <Button
                        onPress={() => onButton()}
                    >
                        <Ionicons name={'md-arrow-round-back'} size={HEIGHT(26)} color="#FFFF" />
                    </Button>
                )}
            />
            <Title style={{ color: '#ffff' }}>{content.namePlace}</Title>
            <Caption style={{ color: '#ffff' }}>{content.address}</Caption>
        </ImageBackground>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

