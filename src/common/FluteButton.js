import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,Linking
} from 'react-native'
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default FluteButton = (props: any) => {
    const { onButton } = props;
    return (
        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#FAD748' title="Camera 360" 
             onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=vStudio.Android.Camera360')}>
            <Image 
                style={{width:WIDTH(30),height:WIDTH(30)}}
                source={{uri:'https://lh3.googleusercontent.com/ev3sivRB4rH4pr-cPrRWRJB71oHCZK8jlr0CXBYFWsmNqXSWUGYrLlDL5QzwzUZEVGY=s180-rw'}}></Image>
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#5CB7B9' title="B612" 
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.linecorp.b612.android')}>
          <Image 
                style={{width:WIDTH(30),height:WIDTH(30)}}
                source={{uri:'https://lh3.googleusercontent.com/eqkbG00skAcMecPqexaLSlZKlmH8OFlKbFwkZ7e8A78s4_8Fuo8xswrYlz0H4YRD8Xg=s180-rw'}}></Image>
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#ffff' title="U Like" 
          onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.gorgeous.liteinternational')}>
          <Image 
                style={{width:WIDTH(30),height:WIDTH(30)}}
                source={{uri:'https://lh3.googleusercontent.com/kcy-J6uHCgtjk4xZwUkMlj93hBnza4cIN-YZ_dYiwIcf701OgXwFt__-_i6ZKGLzRYA=s180-rw'}}></Image>
          </ActionButton.Item>
        </ActionButton>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

