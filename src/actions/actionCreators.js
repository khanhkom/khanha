
export function incCount() {
    return { type: 'INC_COUNT'};
}

export function decCount() {
    return { type: 'DEC_COUNT'};
}

//Account
export function changeTopic(id) {
    return { 
        type: 'CHANGE_TOPIC',
        id,
    };
}