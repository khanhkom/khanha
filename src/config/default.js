export const TRANG_CHU = 'Trang chủ';
export const CHI_TIET = 'Chi tiết';
export const NAME_PART2 = 'Part 2: Listening';
export const NAME_PART3 = 'Part 3: Dictation';
export const NAME_PART4 = 'Part 4: Pronunciation';

export const LIST_HOT = [
    {
        namePlace: 'Cup of Tea Cafe & Bistro',
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội',
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg',
        coordinates: '21.0251967,105.7966571',
        describe: 'Đúng như tên gọi, đồ uống chính tại COT chính là trà và cafe, tuy nhiên nếu muốn có một bức hình sống ảo siêu chất thì bạn hãy gọi một ấm trà nóng nhé, vừa được chụp ảnh lại vừa được nhâm nhi tách trà nóng hổi thơm ngon, tội gì không thử phải không nào?Nếu như bạn còn đang đau đầu tìm cho mình các quán cafe đẹp ở Hà Nội để chụp ảnh thì chắc chắn đừng bỏ qua COT nhé. Tại đây có đến 3 tầng với các góc khác nhau, nhưng đảm bảo rằng góc nào lên hình cũng chỉ có đẹp trở lên mà thôi.',
        listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'],
        time: '21 giờ trước',
    },
    {
        namePlace: 'Phố bích họa Phùng Hưng',
        address: 'Phố Phùng Hưng, quận Hoàn Kiếm, Hà Nội',
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/07/bo-tui-10-diem-chup-anh-ngoai-canh-dep-o-ha-noi-outfit.jpg',
        coordinates: '21.0251967,105.7966571',
        describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.',
        listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'],
        time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/38536397_753505978314556_1332924576119652352_o-1.jpg',
        namePlace: 'C’est Si Bon',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cafegardenista.jpg',
        namePlace: 'The YLang',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        namePlace: 'Sân bay Gia Lâm',
        address: ' Phúc Đồng, Long Biên Hà Nội',
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/07/dia-diem-chup-anh-dep-o-ha-noi-16.jpg',
        coordinates: '21.0251967,105.7966571',
        describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.',
        listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'],
        time: '21 giờ trước',
    },

];


export const LIST_CAFE = [
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg',
        namePlace: 'Cup of Tea Cafe & Bistro',
        coordinates: '21.0251967,105.7966571',
        phoneNumber:'0839 931 716',
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội',
        describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.',
        listImage: ['https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg','https://images.foody.vn/images/trillgroup.jpg','https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg','https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg','https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/1658_17882898_529408880781045_1811213216533970944_n1.jpg',
        namePlace: 'Cafe Lissom Parlour',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cafegardenista.jpg',
        namePlace: 'The YLang',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/38536397_753505978314556_1332924576119652352_o-1.jpg',
        namePlace: 'C’est Si Bon',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1-1493201399280.jpg',
        namePlace: 'Flatform',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
];
export const LIST_TRA_SUA = [
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/bobapop-dao-tan.jpg',
        namePlace: 'Trà sữa Bobapop – Đào Tấn',
        coordinates: '21.0251967,105.7966571',
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/tocotoco-hangbai.jpg',
        namePlace: 'Trà sữa TocoToco – Hàng Bài',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/gongcha-lythuongkiet.jpg',
        namePlace: 'Trà sữa Gong Cha – Lý Thường Kiệt',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/07/tra-sua-royaltea-thai-phien1.jpg',
        namePlace: 'Trà sữa Royaltea – Thái Phiên',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/07/quan-tra-sua-theteapub14.jpg',
        namePlace: 'Trà sữa TheTeaPub – Lò Sũ',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
]
export const LIST_RAP_CP = [
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg',
        namePlace: 'Cafe phim Hà Đông',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-T-Box.jpg',
        namePlace: 'T-Box Cafe Phim',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Kinomax.jpg',
        namePlace: 'Kinomax Cafe Phim 3D/HD',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Paradise-Cafe-Phim.jpg',
        namePlace: 'Paradise Coffee & Film',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Ho-tung-mau-2.jpg',
        namePlace: 'Cafe phim 3D Hồ Tùng Mậu',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
]
export const LIST_HOME_STAY = [

    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Mei-Hideaway.jpg',
        namePlace: 'Mei Hideaway',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Lagom-Homestay-1.jpg',
        namePlace: 'Lagom Homestay',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://scontent.fhan1-1.fna.fbcdn.net/v/t1.0-9/36652441_403569323485035_1877483015723024384_n.jpg?_nc_cat=106&_nc_oc=AQnV4Ujj8ZnEoFuw82dSqlGG7Ct6hNPUIDEJjIzuZz_wL42i5YWECHwFg7qX7EfIWnE&_nc_ht=scontent.fhan1-1.fna&oh=7779b2bd318820e45df4f45b3ab3616f&oe=5D3BA4C3',
        namePlace: 'Arena Village',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Tropical-Nordic-House.jpg',
        namePlace: 'Tropical Nordic House',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Au-frais.jpg',
        namePlace: 'Au frais',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: ['https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg','https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg','https://media.ex-cdn.com/EXP/media.giadinhmoi.vn/files/ailinh/2017/12/21/cup-of-tea-19-1444.jpg'], time: '21 giờ trước',
    },
]
export const DATA_TOPIC = [
    {
        source: 'http://2sao.vietnamnetjsc.vn/images/2017/10/19/09/21/kieu-trinh.jpg',
        title: 'Mẹo nhỏ sống ảo',
        selected: false,
    },
    {
        source: 'https://znews-photo.zadn.vn/w660/Uploaded/jaroin/2016_12_06/1.jpg',
        title: 'Quán cà phê',
        selected: false,
    },
    {
        source: 'http://xuongbanghecafe.com/wp-content/uploads/2017/10/ban-ghe-Quan-tra-sua-dep-2.jpg',
        title: 'Quán trà sữa',
        selected: false,
    },
    {
        source: 'https://www.flynow.vn/blog/wp-content/uploads/2017/08/20768108_1535042529888577_6126814775052528550_n.jpg',
        title: 'Homestay',
        selected: false,
    },
    {
        source: 'https://images.foody.vn/images/12642880_1109539962413140_5293999953244283149_n.jpg',
        title: 'Rạp chiếu phim mini',
        selected: false,
    },
    {
        source: 'http://static2.yan.vn/YanNews/2167221/201709/20170924-100752-20170922-105857-duc_8963-edit_600x424_600x424.jpg',
        title: 'Khác',
        selected: false,
    },
]
export const LIST_DATA_TOPIC = [
    {
        title: 'Điểm Mặt Những Quán Cà Phê Đẹp Mê Ly',
        data: LIST_CAFE,
    },
    {
        title: 'Check-in Những Quán Trà Sữa Cực Chất',
        data: LIST_TRA_SUA,
    },
    {
        title: 'Lạc Vào Trong Những Homestay Đầy Thơ Mộng',
        data: LIST_HOME_STAY,
    },
    {
        title: 'Hẹn Hò Tại Rạp Chiếu Phim mini Đầy Lãng Mạn',
        data: LIST_RAP_CP,
    },
]
