//import lib
import {  createStackNavigator,createAppContainer } from 'react-navigation';
import React from 'react';

import Details from '../container/Details/Details';
import Home from '../container/Home/Home';
import SelectTopic from '../container/SelectTopic/SelectTopic';
import ViewAll from '../container/ViewAll/ViewAll';
import ScreenMap from '../container/Maps/ScreenMap';
import Test from '../container/Maps/Test'
console.disableYellowBox = true
const AppNavigator = createStackNavigator({
	Details: { screen: Details },
	SelectTopic: { screen: SelectTopic },
	ViewAll: { screen: ViewAll },
	Home: { screen: Home },
	//khanhthem
	ScreenMap:{screen:ScreenMap},
	Test:{screen:Test}
	
}, {
		initialRouteName: 'ScreenMap',
		swipeEnabled: true,
		animationEnabled: false,
		headerMode: 'none',
		navigationOptions: {
			header: null
		},
		lazy: true,
		cardStyle: {
			backgroundColor: '#FFF',
			opacity: 1
		},
	})
const Router=createAppContainer(AppNavigator);
export default Router;
