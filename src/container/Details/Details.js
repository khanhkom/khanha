/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, ScrollView
} from 'react-native'

import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common
import { ImageBackground, Text, NavigationBar, Title, Button, Caption, Heading, Subtitle } from '@shoutem/ui';

import HeaderReal from '../../common/HeaderReal';
import { CHI_TIET } from '../../config/default';
import { WIDTH, HEIGHT } from '../../config/Function';
import ItemForDetails from '../../common/ItemForDetails';

class Details extends Component<Props> {
    render() {
        let content =  this.props.navigation.getParam('content');
        return (
            <ScrollView style={styles.container}>
                <Swiper
                    autoplay
                    style={{
                        width: WIDTH(360), height: HEIGHT(220),
                    }}>
                    {
                        content.listImage.map((value, index) => {
                            return (
                                <ItemForDetails
                                    onButton={() => this.props.navigation.goBack()}
                                    linkImage={value}
                                    content={content}
                                />
                            )
                        })
                    }
                </Swiper>
                <View style={styles.cntMain}>
                    <Title style={{ width: WIDTH(250) }}>{content.namePlace}</Title>
                    <Caption>Địa chỉ: {content.address}</Caption>
                    <Caption>Số điện thoại: {content.phoneNumber}</Caption>
                    <View style={{
                        flex: 0,
                        marginTop: HEIGHT(10),
                        paddingHorizontal: WIDTH(5),
                    }}>
                        <Subtitle style={{ color: '#000' }}>     {content.describe}</Subtitle>
                    </View>

                </View>
                <View style={{
                    flex: 0,
                    paddingHorizontal: WIDTH(10),
                    marginTop: HEIGHT(20),
                }}>
                    {/* <Title>Một số góc ảnh đẹp gợi ý cho bạn: </Title> */}

                </View>
            </ScrollView>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(Details);