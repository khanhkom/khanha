import React, { Component } from 'react';
import {
    View,
    Text,
    Image, Alert,
    StyleSheet, TouchableOpacity,
    Dimensions

} from 'react-native'
import geolib from "geolib"
import MapView from 'react-native-maps';
const { height, width } = Dimensions.get('window');
import Detail from './Detail';
import styles from './styles';
var DATA = [
    {
        id: 0,
        name: "Co.op Mart Hà Đông",
        marker: {
            latitude: 20.9826962,
            longitude: 105.7879711
        },
        image: [
            require('./coopmart1.jpg'),
            require('./coopmart2.jpg'),
            require('./coopmart3.jpg')
        ],
        Address:"143 Nguyễn Trãi,Thanh Xuân,Hà Nội",
        PhoneNumber:"033227799",
        Descriptions: "Siêu thị",
        details: "DetailPlant.js",
    },
    {
        id: 1,
        name: "Học viện CNBCVVT",
        marker: {
            latitude: 21.0087199,
            longitude: 105.7732745
        },
        image: [require('./ptit1.jpg'),
        require('./ptit2.jpg'),
        require('./ptit3.jpg')
        ],
        Address:"143 Nguyễn Trãi,Thanh Xuân,Hà Nội",
        PhoneNumber:"033227799",
        Descriptions: "Trường học",
        details: "DetailPlant.js",
    },
    {
        id: 2,
        name: "Đại học kiến trúc HN",
        marker: {
            latitude: 20.9805524,
            longitude: 105.7871778
        },
        image: [require('./kientruc1.jpg'),
        require('./kientruc2.jpg'),
        require('./kientruc3.jpg')
        ],
        Address:"143 Nguyễn Trãi,Thanh Xuân,Hà Nội",
        PhoneNumber:"033227799",
        Descriptions: "Trường Học",
        details: "DetailPlant.js",
    },
    {
        id: 3,
        name: "Đại Học Y Hà Nội",
        marker: {
            latitude: 21.0032249,
            longitude: 105.8284719
        },
        image: [require('./YHN1.jpg'),
        require('./YHN2.jpg'),
        require('./YHN3.jpg')],
        Address:"143 Nguyễn Trãi,Thanh Xuân,Hà Nội",
        PhoneNumber:"033227799",
        Descriptions: "Trường Học",
        details: "DetailPlant.js",
    }
]
export default class ShowMap extends Component {

    constructor(props) {
        super(props)
        arrayMarkers = [{
            latitude: 20.9822573,
            longitude: 105.7888341,
        }],
            showDetail= false,
            arrayIdnear = [],
            this.state = {
                idnear: [],
                region: {
                    latitude: 20.9822573,
                    longitude: 105.7888341,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01,
                },
                markers: arrayMarkers,
                location: {
                    latitude: 20.9822573,
                    longitude: 105.7888341,
                }
            }
    }
    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                //   const location = JSON.stringify(position);     
                for (marker of DATA) {
                    if (geolib.getDistance(position.coords, {
                        latitude: marker.marker.latitude,
                        longitude: marker.marker.longitude
                    }) < 10000) {
                        arrayIdnear.push({
                            id: marker.id
                        })
                    }
                }
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.01,
                        longitudeDelta: 0.01,
                    },
                    location: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                    idnear: arrayIdnear
                });
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 3600000 }
        );
    }
    GetRange() {
        Alert.alert('Get range')
    }
    onRegionChange(data) {

    }
    showDetail(Places){
        this.setState({
            Place:Places,
            showDetail:true
        })
    }
    onPress(data) {
        let latitude = data.nativeEvent.coordinate.latitude;
        let longitude = data.nativeEvent.coordinate.longitude;
        arrayMarkers.push({
            latitude: latitude,
            longitude: longitude,
        }
        )
        this.setState({
            showDetail:false,
            markers: arrayMarkers
        })
        //console.log(this.state.)
    }
    renderMarkerNear() {
        markers = [];
        for (marker of this.state.idnear) {
            markers.push(
                <MapView.Marker key={DATA[marker.id].marker.latitude} coordinate={DATA[marker.id].marker} 
                onPress={(e) => {e.stopPropagation(); this.showDetail(DATA[marker.id])}}
                >
                    <MapView.Callout
                        tooltip={true}>
                        <View style={{ height: 70, width: 70 }}>
                            <Image source={require('./ptit1.jpg')} style={{ height: 30, width: 30 }}></Image>
                            {/* <Text>{DATA[marker.id].name}</Text> */}
                        </View>
                    </MapView.Callout>
                </MapView.Marker>
            )
        }
        return markers;
    }
    renderMarker() {
        markers = [];
        for (marker of this.state.markers) {
            markers.push(
                <MapView.Marker key={marker.latitude} coordinate={marker} title={'Local' + marker.longitude} description={'Detail'} />
            )
        }
        return markers;
    }
    render() {
        return (
            <View style={styles.container}>
                <MapView style={styles.map}
                    initialRegion={this.state.region}
                    onRegionChange={this.onRegionChange.bind(this)}
                    onPress={this.onPress.bind(this)}
                >
                    <MapView.Marker
                        image={require('./pin.png')}
                        key={this.state.location.latitude} coordinate={this.state.location} title={'My Location'} description={''} ></MapView.Marker>
                    {this.renderMarkerNear()}
                </MapView>
                {this.state.showDetail?<Detail Place={this.state.Place}/>:[]}
            </View>
        )
    }
}