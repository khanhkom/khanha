import React, { Component } from 'react';
import {
    View,
    Text,
    Image, Alert,
    StyleSheet, TouchableOpacity,
    Dimensions

} from 'react-native'
import geolib from "geolib"
import MapView from 'react-native-maps';
import Callout from 'react-native-maps';
import OverlayComponent from 'react-native-maps';
const { height, width } = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Swiper from 'react-native-swiper';
import Entypo from 'react-native-vector-icons/Entypo';
import { ImageBackground, NavigationBar, Title, Button, Caption, Heading, Subtitle } from '@shoutem/ui';
import styles from './styles';
import { WIDTH, HEIGHT } from '../../config/Function';
export default class Detail extends Component{
    render(){
        const{Place}=this.props;
        return(
            <View style={styles.buttonContainer}>
                    <View style={styles.swiperStyle} >
                        <Swiper style={styles.swiperStyle} showsButtons={true}>
                            <View style={styles.imageStyle}>
                                <Image source={Place.image[0]} style={styles.imageStyle}></Image>
                            </View>
                            <View style={styles.imageStyle}>
                                <Image source={Place.image[1]} style={styles.imageStyle}></Image>
                            </View>
                            <View style={styles.imageStyle}>
                                <Image source={Place.image[2]} style={styles.imageStyle}></Image>
                            </View>
                        </Swiper>
                    </View>
                    <View style={{ width: 320 / 360 * width, height: 1 / 8 * height }}>
                        <Title style={{ width: WIDTH(250) }}>{Place.name}</Title>
                        <Caption>Địa chỉ: {Place.Address} </Caption>
                        <Caption>Số điện thoại: {Place.PhoneNumber}</Caption>
                        <Caption>Mô tả: {Place.Descriptions}</Caption>
                    </View>
                    <View style={{ height: 1 / 21 * height, width: 320 / 360 * width, flexDirection: 'row' }}>
                        <TouchableOpacity style={{ height: 30 / 640 * height, width: 112 / 360 * width, backgroundColor: '#1A73E8', borderRadius: 13, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', elevation: 2 }}>
                            <FontAwesome name="share-square" color="#fff" size={20} />
                            <Text style={{ color: '#fff', marginLeft: 2 / 360 * width }}>Chỉ đường</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ height: 30 / 640 * height, width: 112 / 360 * width, backgroundColor: '#fff', borderRadius: 13, flexDirection: 'row', marginLeft: 10, borderWidth: 1, borderColor: '#1A73E8', justifyContent: 'center', alignItems: 'center', elevation: 2 }}>
                            <Entypo name="paper-plane" color="#1A73E8" size={20} />
                            <Text style={{ color: '#1A73E8', marginLeft: 2 / 360 * width }}>Chi tiết</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        )
    }

}