import {
    StyleSheet,
    Dimensions
 } from 'react-native'
 
 //import
 import IMAGE from '../../assets/image';
 import STYLES from '../../config/styles.config';
 import colors from '../../config/colors';
const { height, width } = Dimensions.get('window');
 export default styles = StyleSheet.create({

    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 200,
        alignItems: 'stretch',
    },
    button: {
        width: 100,
        paddingHorizontal: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
    },
    buttonContainer: {
        width: 340 / 360 * width,
        height: 1 / 3 * height,
        marginVertical: 20,
        //backgroundColor: 'transparent',
        backgroundColor: 'white',
        borderRadius: 13,
        elevation: 1,
        alignItems: 'center',
        //  justifyContent: 'center',
    },
    buttonText: {
        textAlign: 'center',
    },
    swiperStyle: {
        height: 1 / 7 * height,
        width: 340 / 360 * width,
        elevation: 2,
        borderTopLeftRadius:13,
        borderTopRightRadius:13,
    },
    //
    wrapper: {
    },
imageStyle:{
    height: 1 / 7 * height,
    width: 340 / 360 * width,
    borderTopLeftRadius:13,
    borderTopRightRadius:13,
}
});
 