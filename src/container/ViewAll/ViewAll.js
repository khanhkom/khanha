/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import { LIST_CAFE } from '../../config/default';
import ItemPost from '../../common/ItemPost';
import { HEIGHT, WIDTH } from '../../config/Function';
import ItemForAll from '../../common/ItemForAll';

class ViewAll extends Component<Props> {
    onPost = (content) => {
        this.props.navigation.navigate('Details',{content:content})
    }
    render() {
        let content = this.props.navigation.getParam('content');
        return (
            <View style={styles.container}>
                <HeaderReal
                    title={content.title}
                    onButtonLeft={() => this.props.navigation.goBack()}
                    onButtonRight={() => { }}
                    iconLeft={'ios-arrow-back'}
                    iconRight={''}
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{flex:1}}>
                    <View style={{
                        flex: 1,
                        width:WIDTH(360),
                        marginTop: HEIGHT(10),
                    }}>
                        {
                            content.data.map((value, index) => {
                                return (
                                    <ItemForAll
                                        key={index}
                                        content={value}
                                        onButton={()=>this.onPost(value)}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
                <FluteButton />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(ViewAll);