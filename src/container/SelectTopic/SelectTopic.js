/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import { CHI_TIET } from '../../config/default';
import ViewWelcome from '../../common/SelectTopic/ViewWelcome';
import ListImage from '../../common/SelectTopic/ListImage';
import { changeTopic } from '../../actions/actionCreators'
class SelectTopic extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            countTopic: 0,
        }
    }
    render() {
        const { selectedTopic } = this.props;
        return (
            <View style={styles.container}>
                <ViewWelcome
                    disabled={this.state.countTopic >= 3 ? false : true}
                    onButton={() => this.props.navigation.navigate("Home")}
                />
                <ListImage
                    data={selectedTopic}
                    onButton={(id) => {
                        let countTopic=selectedTopic[id].selected ? this.state.countTopic-1 : this.state.countTopic+1;
                        this.setState({ countTopic })
                        this.props.changeTopic(id)
                    }}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        selectedTopic: state.accountReducer.selectedTopic,
    };
}

export default connect(mapStateToProps, {
    changeTopic
})(SelectTopic);