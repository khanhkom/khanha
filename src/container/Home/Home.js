/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    StyleSheet, Text, TouchableOpacity, View, StatusBar
} from 'react-native'
import { NavigationBar, Icon, Title } from '@shoutem/ui'

import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import { HEIGHT, WIDTH } from '../../config/Function';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import { LIST_DATA_TOPIC, LIST_CAFE, LIST_TRA_SUA, LIST_HOME_STAY, LIST_RAP_CP, LIST_HOT } from '../../config/default';
import { incCount, decCount } from '../../actions/actionCreators'
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import ItemSearch from '../../common/ItemSearch';
import { ScrollView } from 'react-native-gesture-handler';
import ListPost from '../../common/Home/ListPost';
import ListSelfie from '../../common/Home/ListSelfie';
import FluteButton from '../../common/FluteButton';

class Home extends Component<Props> {
    state = {
        search: '',
    };

    updateSearch = search => {
        this.setState({ search });
    };
    onPost = (content) => {
        this.props.navigation.navigate('Details',{content:content})
    }
    onViewAll = (id) => {
        if (id==0) this.props.navigation.navigate('ViewAll',{content:{
            title:'Quán cà phê',
            data:LIST_CAFE,
        }})
        if (id==1) this.props.navigation.navigate('ViewAll',{content:{
            title:'Quán trà sữa',
            data:LIST_TRA_SUA,
        }})
        if (id==2) this.props.navigation.navigate('ViewAll',{content:{
            title:'Homestay',
            data:LIST_HOME_STAY,
        }})
        if (id==3) this.props.navigation.navigate('ViewAll',{content:{
            title:'Rạp chiếu phim mini',
            data:LIST_RAP_CP,
        }})
    }
    
    render() {
        const { search } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.lightGrayEEE} />
                <ItemSearch
                    title={'Tìm kiếm địa điểm...'}
                    content={search}
                    onButton={this.updateSearch}
                    
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{
                        flex: 1,
                    }}>
                    <View style={styles.cntMain}>
                        <ListSelfie
                            data={LIST_HOT}
                            onButton={this.onPost}
                        />
                        {
                            LIST_DATA_TOPIC.map((value, index) => {
                                return (
                                    <ListPost
                                        key={index}
                                        index={index}
                                        title={value.title}
                                        data={value.data}
                                        onButton={this.onPost}
                                        onViewAll={(id)=> this.onViewAll(id)}
                                    />
                                )
                            })
                        }

                    </View>
                </ScrollView>
                <FluteButton
                    onButton={() => { }}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
    incCount, decCount
})(Home);