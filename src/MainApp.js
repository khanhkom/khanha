//Import lib
import store from './store/store';
import { Provider } from "react-redux";
import React from 'react';
import Router from './router/Router';


export default class MainApp extends React.PureComponent<Props> {
	render() {
		return (
			<Provider store={store}>
					<Router />
			</Provider>
		);
	}
}
